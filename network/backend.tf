terraform {
  backend "s3" {
    bucket = "final-assessment"
    key    = "env/final-assessment/network.tfstate"
    region = "us-east-1"
  }
}