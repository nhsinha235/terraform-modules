output "redis_sg_id" {
  description = "The ID of redis SG"
  value       = module.redis_ec2.private_ec2_sg_id
}

output "private_ec2_ip" {
  description = "This is the private of ec2"
  value       = module.redis_ec2.*.private_ec2_ip
}