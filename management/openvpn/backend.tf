data "terraform_remote_state" "network" {
  backend = "s3"
  config = {
    bucket = "final-assessment"
    key    = "env/final-assessment/network.tfstate"
    region = "us-east-1"
  }
}

terraform {
  backend "s3" {
    bucket = "final-assessment"
    key    = "env/final-assessment/openvpn.tfstate"
    region = "us-east-1"
  }
}