output "redis_nat_id" {
  description = "redis nat id"
  value       = aws_nat_gateway.redis_nat.id
}
