resource "aws_eip" "nat_ip" {                                   ## elastic ip
  vpc = true
}

resource "aws_nat_gateway" "redis_nat" {                         ## nat gatway
  allocation_id = aws_eip.nat_ip.id
  subnet_id     = var.pub_sub_id
  tags = {
    Name = "nat_gateway"
  }
}